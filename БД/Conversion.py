from tapi_yandex_metrika import YandexMetrikaStats
import sqlite3

def conversion_to_DB(METRIC_ACCESS_TOKEN, METRIC_IDS, start_date, end_date, con):
    api = YandexMetrikaStats(
        access_token=METRIC_ACCESS_TOKEN,
        receive_all_data=True
    )
    params = {
        'ids': METRIC_IDS,
        'metrics': 'ym:s:visits',
        'dimensions': 'ym:s:date, ym:s:goal',
        'date1': start_date,
        'date2': end_date,
        'sort': 'ym:s:date',
        'accuracy': 'full',
        'limit': 100000
    }
    result = api.stats().get(params=params)
    result = result().data
    result = result[0]['data']

    cursor = con.cursor()
    for i in range(len(result)):
        date = result[i]['dimensions'][0]['name']
        goal = result[i]['dimensions'][1]['name']
        visits = result[i]['metrics'][0]
        cursor.execute('''
            INSERT INTO Conversion (date, goal, visits)
            VALUES (?, ?, ?)
        ''', (date, goal, visits))

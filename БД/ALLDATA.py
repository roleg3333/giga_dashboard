import sqlite3
from tapi_yandex_metrika import YandexMetrikaStats
import Devices, TrafficSource, BrandTraffic, Conversion, ClickMap, SearchSystem, Positions
from datetime import date



#параметры для ЯндексМетрики
METRIC_ACCESS_TOKEN = "y0_AgAAAABu9NRrAAog4AAAAADm7BmOrOpllZVrRHuLg0S7Sshfz-wVSCU"
METRIC_IDS = "12418261"
start_YM_date = '30daysAgo'
end_YM_date = 'today'


#параметры для TopVizor'a
TV_projectId = 7784199
TV_token = '46d84eaa08c50379ce6b59607e0d5b79'
TV_userId = '358921'
start_TV_date = '2023-01-01'
end_TV_date = str(date.today())


    
#---создание общей базы данных
con = sqlite3.connect("БД/ourDB.db")
cursor = con.cursor()

#создание таблицы для статистики по устройствам
cursor.execute("""CREATE TABLE Devices
                (date Text,  
                userDevice TEXT, 
                visits INTEGER)
            """)

#создание таблицы для статистики по источникам трафика
cursor.execute("""CREATE TABLE TrafficSource
                (date Text,  
                traffic_source TEXT, 
                visits INTEGER)
            """)

#создание таблицы для статистики по доле брендового и небрендового трафика
cursor.execute("""CREATE TABLE BrandTraffic
                (date integer,  
                searchPhrase TEXT, 
                visits INTEGER)
            """)
#создание таблицы для статистики по конверсии
cursor.execute("""CREATE TABLE Conversion
                (date integer,  
                goal TEXT, 
                visits INTEGER)
            """)
#создание таблицы для для карты кликов 
cursor.execute("""CREATE TABLE Clickmap
                (date integer,  
                URL TEXT, 
                visits INTEGER)
            """)
#создание таблицы для поисковых систем
cursor.execute("""CREATE TABLE SearchSystem
                (date integer,  
                searchsys TEXT, 
                visits INTEGER)
            """)
            
#Загрузка данных во все таблицы
Devices.devices_to_DB(METRIC_ACCESS_TOKEN, METRIC_IDS, start_YM_date, end_YM_date,con)
TrafficSource.TrafficSource_to_DB(METRIC_ACCESS_TOKEN, METRIC_IDS, start_YM_date, end_YM_date,con)
BrandTraffic.brand_to_DB(METRIC_ACCESS_TOKEN, METRIC_IDS, start_YM_date, end_YM_date,con)
Conversion.conversion_to_DB(METRIC_ACCESS_TOKEN, METRIC_IDS, start_YM_date, end_YM_date,con)
ClickMap.click_to_DB(METRIC_ACCESS_TOKEN, METRIC_IDS, start_YM_date, end_YM_date,con)
SearchSystem.search_to_DB(METRIC_ACCESS_TOKEN, METRIC_IDS,start_YM_date, end_YM_date,con)
Positions.position_to_DB(TV_projectId, TV_token, TV_userId, start_TV_date, end_TV_date, con)
con.commit()
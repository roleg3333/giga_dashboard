from tapi_yandex_metrika import YandexMetrikaStats
import sqlite3

def search_to_DB(METRIC_ACCESS_TOKEN, METRIC_IDS, start_date, end_date, con):
    api = YandexMetrikaStats(
        access_token=METRIC_ACCESS_TOKEN,
        receive_all_data=True
    )

    params = {
        'ids': METRIC_IDS,
        'metrics': 'ym:s:visits',
        'dimensions': 'ym:s:date, ym:s:<attribution>SourceEngine,ym:s:<attribution>TrafficSource',
        'date1': start_date,
        'date2': end_date,
        'sort': 'ym:s:date',
        'accuracy': 'full',
        'limit': 100000
    }

    result = api.stats().get(params=params)
    result = result().data
    result = result[0]['data']

    cursor = con.cursor()
    for i in range(len(result)):
        if result[i]['dimensions'][2]['name']=='Search engine traffic':
            date = result[i]['dimensions'][0]['name']
            searchsys = result[i]['dimensions'][1]['name']
            visits = result[i]['metrics'][0]

            cursor.execute('''
                INSERT INTO SearchSystem (date, searchsys, visits)
                VALUES (?, ?, ?)
            ''', (date, searchsys, visits))

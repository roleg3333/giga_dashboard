from tapi_yandex_metrika import YandexMetrikaStats
import json
import pandas as pd

ACCESS_TOKEN = "y0_AgAAAABu9NRrAAog4AAAAADm7BmOrOpllZVrRHuLg0S7Sshfz-wVSCU"
METRIC_IDS = "12418261"

# По умолчанию возвращаются только 10000 строк отчета, 
# если не указать другое кол-во в параметре limit.
# В отчете может быть больше строк, чем указано в limit 
# Тогда необходимо сделать несколько запросов для получения всего отчета.
# Чтоб сделать это автоматически вы можете указать 
# параметр receive_all_data=True при инициализации класса.

#Параметры запроса для библиотеки tapi_yandex_metrika
api = YandexMetrikaStats(
    access_token=ACCESS_TOKEN, 
    # Если True, будет скачивать все части отчета. По умолчанию False.
    receive_all_data=True
)

#Параметры запроса для библиотеки tapi_yandex_metrika
params = dict(
    ids = METRIC_IDS,
    metrics = "ym:s:visits",
    dimensions = "ym:s:date, ym:s:deviceCategoryName",
    date1 = "30daysAgo",
    date2 = "today",
    sort = "ym:s:date",
    accuracy="full",
    limit = 500
)
#Получаем данные из Yandex.Metrika API
result = api.stats().get(params=params)
result = result().data
result = result[0]['data']

#Создаем пустой dict (словать данных)
dict_data = {}
#Парсим исходный list формата Json в dictionary (словарь данных)
for i in range(0, len(result)):
        dict_data[i] = {
            'date':result[i]["dimensions"][0]["name"],
            'userDevice': result[i]["dimensions"][1]["name"],
            'visits':result[i]["metrics"][0],
            }
    
#Создаем DataFrame из dict (словаря данных или массива данных)
dict_keys = dict_data[0].keys()
df = pd.DataFrame.from_dict(dict_data, orient='index',columns=dict_keys)

#Выгрузка данных из DataFrame в Excel
df.to_excel("Тип устройств/Устройства.xlsx",
        sheet_name='data',
        index=False)
        
import pandas as pd
from openpyxl import Workbook
from datetime import date
import requests

projectId = 7784199  # ID вашего проекта

headers = {
    'Content-Type': 'application/json',
    'User-Id': '358921',
    'Authorization': 'bearer 46d84eaa08c50379ce6b59607e0d5b79',
}

# Получение списка регионов проекта
regions_indexes = []
regionsSelectorData = {
    'id': projectId,
    'show_searchers_and_regions': 1
}

regionsSelector = requests.post('https://api.topvisor.com/v2/json/get/projects_2/projects', headers=headers, json=regionsSelectorData)
regionsSelectorResult = regionsSelector.json().get('result')

if regionsSelectorResult:
    project = regionsSelectorResult[0]
    for searcher in project['searchers']:
        for region in searcher['regions']:
            regions_indexes.append(region['index'])

    # Получение позиций для всех регионов проекта
    positionSelectorData = {
        'project_id': projectId,
        'regions_indexes': regions_indexes,
        'date1': '2023-01-01',
        'date2': str(date.today()),
        'count_dates': 10,
        'show_exists_dates': 1,
        'show_headers': 1
    }

    positionsSelector = requests.post('https://api.topvisor.com/v2/json/get/positions_2/history', headers=headers, json=positionSelectorData)
    positionsSelectorResult = positionsSelector.json().get('result')

    if positionsSelectorResult:
        dates = positionsSelectorResult['headers']['dates']
        projects = positionsSelectorResult['headers']['projects']
        keywords = positionsSelectorResult['keywords']

        # Создание пустой таблицы Excel
        wb = Workbook()
        ws = wb.active

        # Запись заголовков
        headers = ['keywords'] + dates
        ws.append(headers)

        for project in projects:
            for searcher in project['searchers']:
                for region in searcher['regions']:
                    # Запись информации о проекте, поисковике и регионе
                    for keyword in keywords:
                        # Запись ключевого слова и позиций для каждой даты
                        row = [keyword['name']]
                        for date in dates:
                            qualifiers = f"{date}:{project['id']}:{region['index']}"
                            if qualifiers in keyword['positionsData']:
                                positions = keyword['positionsData'][qualifiers]['position']
                                row.append(positions) if positions else row.append('--')
                            else:
                                row.append('')
                        ws.append(row)

        # Сохранение таблицы в файл
        filename = 'Позиция сайта/Динамика.xlsx'
        wb.save(filename)
        print(f"Данные сохранены в файл: {filename}")
    else:
        errors = positionsSelector.json().get('errors')
        if errors:
            print(errors)
else:
    errors = regionsSelector.json().get('errors')
    if errors:
        print(errors)

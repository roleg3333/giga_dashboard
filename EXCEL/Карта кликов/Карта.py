from tapi_yandex_metrika import YandexMetrikaStats
import pandas as pd

# Ваш токен доступа к API Яндекс.Метрики
ACCESS_TOKEN = "y0_AgAAAABu9NRrAAog4AAAAADm7BmOrOpllZVrRHuLg0S7Sshfz-wVSCU"

# ID счетчика Яндекс.Метрики
METRIC_ID = "12418261"

# Инициализация клиента для работы с API Яндекс.Метрики
api = YandexMetrikaStats(access_token=ACCESS_TOKEN)

# Параметры запроса для получения информации о переходах на сайте
params = {
    "ids": METRIC_ID,
    "metrics": "ym:s:visits",
    "dimensions": "ym:s:startURLPath",
    "date1": "2023-01-01",
    "date2": "today",
    "sort": "-ym:s:visits",
    "limit": 10
}

# Получение данных из Яндекс.Метрики
response = api.stats().get(params=params)
data = response().data

# Создание пустого словаря данных
dict_data = {}

# Парсинг данных и сохранение в словарь
for i in range(len(data)):
    dict_data[i] = {
        "url": data[i]["dimensions"][0]["name"],
        "visits": data[i]["metrics"][0]
    }

# Создание DataFrame из словаря данных
df = pd.DataFrame.from_dict(dict_data, orient="index")

# Вывод DataFrame с информацией о ссылках
print(df)

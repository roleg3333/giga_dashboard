import flet as ft
from flet import Row
def main(page: ft.Page):
    page.bgcolor=ft.PaintLinearGradient(0,300, [])
    t = ft.Text(value="SEO Дашборд", size=25, text_align=ft.TextAlign.CENTER, color="white")
    TodayButton = ft.ElevatedButton("Сегодня",style=ft.ButtonStyle(shape=ft.RoundedRectangleBorder(radius=10)), bgcolor="white",color='#352958', width=135) 
    YesterdayButton = ft.ElevatedButton("Вчера",style=ft.ButtonStyle(shape=ft.RoundedRectangleBorder(radius=10)), bgcolor="white",color='#352958', width=130)
    WeekButton = ft.ElevatedButton("Неделя", style=ft.ButtonStyle(shape=ft.RoundedRectangleBorder(radius=10)), bgcolor="white",color='#352958', width=130)
    MonthButton = ft.ElevatedButton("Месяц", style=ft.ButtonStyle(shape=ft.RoundedRectangleBorder(radius=10)), bgcolor="white",color='#352958', width=130)
    KvartButton = ft.ElevatedButton("Квартал", style=ft.ButtonStyle(shape=ft.RoundedRectangleBorder(radius=10)), bgcolor="white",color='#352958', width=130)
    YearButton = ft.ElevatedButton("Год", style=ft.ButtonStyle(shape=ft.RoundedRectangleBorder(radius=10)), bgcolor="white",color='#352958', width=123)
    page.add(
                Row([t], alignment="center"),
                Row([TodayButton, YesterdayButton, WeekButton, MonthButton, KvartButton, YearButton], spacing=2)
            )

    page.update()
    
ft.app(target=main, view=ft.WEB_BROWSER, port=5000)
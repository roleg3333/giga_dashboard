import flet as ft 
def main(page: ft.Page):
    page.fonts = {
        'Panton': "https://bestfonts.pro/fonts_files/5c3baf5bc8e4b404a0291513/files/Panton-Regular.ttf"
    }
    page.update()
    t= ft.Text("Мoop", size=25, font_family='Panton')
    page.add(t)
    

ft.app(target=main, view=ft.AppView, port=5000)